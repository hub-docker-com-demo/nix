# nix

The Nix package manager.  https://nixos.org/nix/

[![Packaging status](https://repology.org/badge/vertical-allrepos/nix.svg)](https://repology.org/project/nix/versions)
